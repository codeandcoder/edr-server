
const WebSocket = require('ws');
const ServerSocket = require('./node/sockets/server-socket')
const EDRServer  = require('./node/edr-server.js')

const wss = new WebSocket.Server({port: 3000});
const serverSocket = new ServerSocket(wss)
const server = new EDRServer(serverSocket)
console.log('EDR Server listening on port 3000')