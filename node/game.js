var Enums = require('./enums')

module.exports = class Game {
    constructor(edrServer, advManager, gameCode) {
        this.edrServer              = edrServer
        this.advManager             = advManager
        this.gameCode               = gameCode
        this.gameStage              = Enums.GameStages.LOBBY
        this.currentAdventure       = undefined
        this.currentEventID         = undefined
        this.chosenOptionID         = undefined
        this.chosenResultIndex      = -1
        this.currentResultTextIndex = -1
        this.currentTextsIndex      = 0
        this.currentPlayerOptions   = undefined
        this.players                = []
        this.nextPlayerNumber       = 0
    }

    setupPlayerActions(playerID) {
        let player = this.edrServer.users[playerID]
        let s = player.socket

        s.on('set-player-ready', (ready) => {
            let pID = this.edrServer.userSockets[s.id]
            this.edrServer.users[pID].ready = ready
            this.emitToAllPlayers('player-update', this.edrServer.users[pID].genUpdateMessage())
        })

        s.on('continue-game', () => {
            if (this.isChoiceTime()) {
                s.emit('game-error', "A choice must be taken")
                return
            }

            let pID = this.edrServer.userSockets[s.id]
            this.edrServer.users[pID].ready = true
            this.emitToAllPlayers('player-update', this.edrServer.users[pID].genUpdateMessage())
            if (this.allPlayersAreReady()) {
                this.resetPlayersReady()
                this.continueGame()
            }
        })

        s.on('choose-option', (optionID) => {
            if (!this.isChoiceTime()) {
                s.emit('game-error', "Not a time for choosing, just continue")
                return
            }

            if (!this.currentPlayerOptions || !this.currentPlayerOptions[optionID]) {
                s.emit('game-error', "Not a valid option")
                return
            }

            // TODO: Check that player has enough energy to pick that option

            let pID = this.edrServer.userSockets[s.id]
            this.edrServer.users[pID].chosenOptionID = optionID
            this.edrServer.users[pID].ready = true
            this.emitToAllPlayers('player-update', this.edrServer.users[pID].genUpdateMessage())
            if (this.allPlayersAreReady()) {
                this.continueGame()
            }
        })

        s.on('start-game', () => {
            if (this.gameStage == Enums.GameStages.PLAYING) {
                s.emit('game-error', "Game already started")
                return
            }

            if (this.players.length < 2) {
                s.emit('game-error', "Minimum number of players is 2")
                return
            }
            
            if (!this.allPlayersAreReady()) {
                s.emit('game-error', "All players must be ready")
                return
            }

            this.resetPlayersReady()
            this.emitToAllPlayers('game-starting')
        })

        s.on('game-screen-loaded', () => {
            let pID = this.edrServer.userSockets[s.id]
            this.edrServer.users[pID].ready = true
            this.emitToAllPlayers('player-update', this.edrServer.users[pID].genUpdateMessage())

            if (this.allPlayersAreReady()) {
                this.resetPlayersReady()
                this.startGame()
            }
        })

        s.on('exit-game', () => {
            let playerGameCode = this.edrServer.users[this.edrServer.userSockets[s.id]].gameCode
            this.edrServer.games[playerGameCode].playerLeft(this.edrServer.userSockets[s.id])
        })
    }

    startGame() {
        this.gameStage = Enums.GameStages.PLAYING
        this.currentAdventure = this.advManager.getRandomAdventure()
        this.currentEventID = this.currentAdventure.getStartingEvent()
        this.chosenOptionID = undefined
        this.chosenResultIndex = -1
        this.currentResultTextIndex = -1
        this.currentTextsIndex = -1
        this.continueGame()
    }

    continueGame() {
        let eventData = this.currentAdventure.getEvent(this.currentEventID)
        if (!this.chosenOptionID) {
            this.currentTextsIndex += 1
            if (this.currentTextsIndex < eventData.texts.length) {
                let isChoice = this.isChoiceTime()
                let advMessage = {
                    "adventureName" : this.currentAdventure.adventureJson.name,
                    "eventTitle" : eventData.name,
                    "isChoice" : isChoice,
                    "eventText" : eventData.texts[this.currentTextsIndex],
                    "players": this.genPlayersData()
                }

                if (isChoice) {
                    this.currentPlayerOptions = this.currentAdventure.getPlayerOptions(this.currentEventID, this.genPlayersData())
                    advMessage["options"] = this.currentPlayerOptions
                } else {
                    this.currentPlayerOptions = undefined
                }

                this.emitToAllPlayers('game-update', advMessage)
            } else {
                this.choiceResolution()
            }
        } else {
            this.currentResultTextIndex += 1
            if (this.currentResultTextIndex < eventData.options[this.chosenOptionID].results[this.chosenResultIndex].texts.length) {
                let advMessage = {
                    "adventureName" : this.currentAdventure.adventureJson.name,
                    "eventTitle" : eventData.name,
                    "isChoice" : false,
                    "eventText" : eventData.options[this.chosenOptionID].results[this.chosenResultIndex].texts[this.currentResultTextIndex],
                    "players": this.genPlayersData()
                }
                this.emitToAllPlayers('game-update', advMessage)
            } else {
                if (eventData.options[this.chosenOptionID].results[this.chosenResultIndex].next
                    && eventData.options[this.chosenOptionID].results[this.chosenResultIndex].next != "") {
                    this.currentEventID = eventData.options[this.chosenOptionID].results[this.chosenResultIndex].next
                    this.chosenOptionID = undefined
                    this.chosenResultIndex = -1
                    this.currentResultTextIndex = -1
                    this.currentTextsIndex = -1
                    this.continueGame()
                } else {
                    this.emitToAllPlayers('game-finished')
                }
            }
        }
    }

    choiceResolution() {
        this.currentPlayerOptions = undefined
        let individualPercentage = 1.0 / this.players.length
        let choicePercentage = {}
        this.players.forEach(pID => {
            let chosenOptionID = this.edrServer.users[pID].chosenOptionID
            if (choicePercentage[chosenOptionID]) {
                choicePercentage[chosenOptionID] += individualPercentage
            } else {
                choicePercentage[chosenOptionID] = individualPercentage
            }
        });
        let choiceList = this.choicesToList(choicePercentage)
        choiceList.sort((a,b) => (a.percentage > b.percentage) ? 1 : ((b.percentage > a.percentage) ? -1 : 0))

        // TODO: If "watch" is the most chosen option, choose randomly between the rest
        // TODO: Select one randomly when multiple have the same percentage
        this.chosenOptionID = choiceList[0].optionID
        let chosenOptionPercentage = choiceList[0].percentage
        let results = this.currentAdventure.getEvent(this.currentEventID).options[this.chosenOptionID].results
        for (let i = 0; i < results.length; i++) {
            if (i+1 == results.length) {
                this.chosenResultIndex = i
            } else {
                let currentResultPercentage = results[i].selection_percentage
                let nextResultPercentage = results[i+1].selection_percentage
                if (chosenOptionPercentage >= currentResultPercentage && chosenOptionPercentage < nextResultPercentage) {
                    this.chosenResultIndex = i
                }
            }
        }
        this.currentResultTextIndex = -1
        this.players.forEach(pID => {
            this.edrServer.users[pID].chosenOptionID = undefined
        });
        console.log("ChosenOptionID = " + this.chosenOptionID + " Chosen result: " + this.chosenResultIndex)
        this.continueGame()
    }

    choicesToList(choices) {
        let list = []
        let keys = Object.keys(choices)
        keys.forEach(k => {
            list.push({
                "optionID" : k,
                "percentage" : choices[k]
            })
        });
        return list
    }

    isChoiceTime() {
        return this.chosenResultIndex == -1 && this.currentTextsIndex == this.currentAdventure.getEvent(this.currentEventID).texts.length-1
    }

    resetPlayersReady() {
        this.players.forEach(pID => {
            this.edrServer.users[pID].ready = false
        });
    }

    allPlayersAreReady() {
        let allReady = true
        this.players.forEach(pID => {
            allReady = allReady && this.edrServer.users[pID].ready
        });
        return allReady
    }

    playerJoined(playerID) {
        this.setupPlayerActions(playerID)
        if (!this.players.includes(playerID))
            this.players.push(playerID)

        this.edrServer.users[playerID].playerNumber = this.nextPlayerNumber
        this.nextPlayerNumber += 1

        this.players.forEach(pID => {
            if (pID != playerID) {
                let p = this.edrServer.users[pID]
                if (p.socket) {
                    p.socket.emit('player-joined', this.edrServer.users[playerID].playerNumber, this.genUpdateMessage())
                }
            }
        })
        let playerData = this.edrServer.users[playerID].genUpdateMessage()
        playerData.playerID = this.edrServer.users[playerID].playerID
        this.edrServer.users[playerID].socket.emit('game-joined', playerData, this.genUpdateMessage())
    }

    playerLeft(playerID) {
        this.players.splice(this.players.indexOf(playerID))
        this.players.forEach(pID => {
            let p = this.edrServer.users[pID]
            if (p.socket) {
                p.socket.emit('player-left', this.edrServer.users[playerID].playerNumber, this.genUpdateMessage())
            }
        })
    }

    emitToAllPlayers(eventName, ...args) {
        let argList = [eventName].concat(args)
        this.players.forEach(pID => {
            this.edrServer.users[pID].socket.emit.apply(this.edrServer.users[pID].socket, argList)
        });
    }

    genPlayersData() {
        let playersData = []
        this.players.forEach(pID => {
            playersData.push(this.edrServer.users[pID].genUpdateMessage())
        })
        return playersData
    }

    genUpdateMessage() {
        let message = {
            "gameCode"  : this.gameCode,
            "gameStage" : this.gameStage,
            "players"   : []
        }
        this.players.forEach(pID => {
            message.players.push(this.edrServer.users[pID].genUpdateMessage())
        })
        if (this.gameStage == Enums.GameStages.PLAYING) {
            let eventData = this.currentAdventure.getEvent(this.currentEventID)
            message["adventure"] = {
                "adventureName" : this.currentAdventure.adventureJson.name,
                "eventTitle" : eventData.name
            }
            // TODO: Improve this message
        }
        return message
    }
}