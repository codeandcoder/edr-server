module.exports = class Adventure {
    constructor(json) {
        this.adventureJson = json
    }

    getPlayerOptions(currentEvent, playersData) {
        let allOptions = this.adventureJson.events[currentEvent].options
        // TODO: Filter Options
        // TODO: Add automatically "watch" option if not replaced
        return allOptions
    }

    getStartingEvent() {
        return this.adventureJson.starting_event
    }

    getEvent(eventID) {
        return this.adventureJson.events[eventID]
    }
}