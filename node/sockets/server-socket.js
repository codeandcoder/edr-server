const { v4: uuidv4 } = require('uuid');
const Socket = require('./socket')

module.exports = class ServerSocket {
    constructor(wss) {
        this.wss = wss
        this.id = uuidv4()
        this.hooks = {}

        wss.on('connection', (ws) => {
            if (this.hooks['connection']) {
                this.hooks['connection'].forEach(f => {
                    f(new Socket(ws))
                });
            }
        })

        wss.on('close', (ws) => {
            if (this.hooks['disconnect']) {
                this.hooks['disconnect'].forEach(f => {
                    f()
                });
            }
        })
    }

    on(eventName, f) {
        if (!this.hooks[eventName])
            this.hooks[eventName] = []
        this.hooks[eventName].push(f)
    }
}