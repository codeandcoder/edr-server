const { v4: uuidv4 } = require('uuid');

module.exports = class Socket {
    constructor(ws) {
        this.ws = ws
        this.id = uuidv4()
        this.hooks = {}

        ws.onopen = (e) => {
            if (this.hooks['connect']) {
                this.hooks['connect'].forEach(f => {
                    f(e)
                })
            }
        }

        ws.onclose = (e) => {
            if (this.hooks['disconnect']) {
                this.hooks['disconnect'].forEach(f => {
                    f(e)
                })
            }
        }

        ws.onmessage = (message) => {
            let mList = JSON.parse(message.data)
            let eventName = mList.shift()
            if (this.hooks[eventName]) {
                this.hooks[eventName].forEach(f => {
                    f.apply(this, mList)
                })
            }
        }
    }

    on(eventName, f) {
        if (!this.hooks[eventName])
            this.hooks[eventName] = []
        this.hooks[eventName].push(f)
    }

    emit(eventName, ...args) {
        console.log("Emit " + eventName + " with ID: " + this.id)
        let mList = [eventName]
        mList = mList.concat(args)
        this.ws.send(JSON.stringify(mList))
    }
}