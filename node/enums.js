module.exports.GameStages = {
    LOBBY       : "lobby",
    PLAYING     : "playing",
    FINISHED    : "finished"
}

module.exports.CharacterClasses = {
    MAGE        : "mage",
    WARRIOR     : "warrior",
    BARD        : "bard",
    RANGER      : "ranger"
}


