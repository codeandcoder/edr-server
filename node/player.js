var Enums = require('./enums')

module.exports = class Player {
    constructor(socket) {
        this.playerID           = ""
        this.playerNumber       = -1
        this.nickname           = ""
        this.gameCode           = ""
        this.ready              = false
        this.socket             = socket
        this.characterClass     = ""
        this.currentHealth      = 0
        this.maxHealth          = 0
        this.currentEnergy      = 0
        this.maxEnergy          = 0
        this.attackValue        = 0
        this.chosenOption       = undefined
    }

    genUpdateMessage() {
        return {
            "playerNumber"      : this.playerNumber,
            "nickname"          : this.nickname,
            "characterClass"    : this.characterClass,
            "currentHealth"     : this.currentHealth,
            "maxHealth"         : this.maxHealth,
            "currentEnergy"     : this.currentEnergy,
            "maxEnergy"         : this.maxEnergy,
            "attackValue"       : this.attackValue,
            "isConnected"       : this.socket && this.socket.connected,
            "ready"             : this.ready
        }
    }
}