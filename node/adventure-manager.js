var fs = require('fs')
var Adventure = require('./adventure')

module.exports = class AdventureManager {
    constructor(adventuresPath) {
        this.adventures = {}

        this.readAdventures(adventuresPath)
    }

    getRandomAdventure() {
        let ids = Object.keys(this.adventures)
        let randomID = ids[Math.floor(Math.random() * ids.length)]
        return this.adventures[randomID]
    }

    readAdventures(adventuresPath) {
        fs.readdir(adventuresPath, (err, files) => {
            if (err) throw err
            files.forEach(f => {
                fs.readFile(adventuresPath + '/' + f, (err, data) => {
                    if (err) throw err
                    var adv = JSON.parse(data)
                    this.adventures[adv.id] = new Adventure(adv)
                })
            });
        })
    }
}