const Enums               = require('./enums')
const Player              = require('./player')
const Game                = require('./game')
const AdventureManager    = require('./adventure-manager')

const adventuresPath = './adventures'

module.exports = class EDRServer {
    constructor(serverSocket) {
        this.games = {}
        this.users = {}
        this.userSockets = {}

        this.advManager = new AdventureManager(adventuresPath)

        serverSocket.on('connection', (socket) => {
            console.log('New socket connection: ' + socket.id)

            socket.on('disconnect', () => {
                if (this.userSockets[socket.id]) {
                    let playerGameCode = this.users[this.userSockets[socket.id]].gameCode
                    this.games[playerGameCode].playerLeft(this.userSockets[socket.id])
                }
                console.log('Socket disconnected: ' + socket.id)
            })

            socket.on('host-game', (playerData) => {
                // TODO: Validate player data
                // TODO: Check if that player is in another game and exit from that other one first

                // Create new Game
                let gameCode = this.generateGameCode()
                let newGame = new Game(this, this.advManager, gameCode)
                this.games[gameCode] = newGame

                // Update player data
                this.updatePlayerData(socket, gameCode, playerData)
                newGame.playerJoined(playerData.playerID)

                console.log('New game hosted: ' + gameCode)
            })

            socket.on('join-game', (gameCode, playerData) => {
                // TODO: Check if that player is in another game and exit from that other one first
                let game = this.games[gameCode]

                if (!game) {
                    socket.emit('game-error', "Game code does not exist.")
                } else {
                    this.updatePlayerData(socket, gameCode, playerData)
                    game.playerJoined(playerData.playerID)
                }
            })
        })
    }

    updatePlayerData(socket, gameCode, playerData) {
        let newPlayer                   = new Player(socket)
        newPlayer.playerID              = playerData.playerID
        newPlayer.gameCode              = gameCode
        newPlayer.nickname              = playerData.nickname
        newPlayer.characterClass        = playerData.characterClass
        newPlayer.currentHealth         = playerData.currentHealth
        newPlayer.maxHealth             = playerData.maxHealth
        newPlayer.currentEnergy         = playerData.currentEnergy
        newPlayer.maxEnergy             = playerData.maxEnergy
        newPlayer.attackValue           = playerData.attackValue
        newPlayer.ready                 = playerData.ready
        this.users[newPlayer.playerID]  = newPlayer
        this.userSockets[socket.id]     = playerData.playerID
    }

    generateGameCode() {
        let letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        let newCode
        do {
            newCode = ""
            for (let i = 0; i < 5; i++) {
                newCode += letters.charAt(Math.floor(Math.random() * letters.length))
            }
        } while (this.games[newCode])
        return newCode
    }
}