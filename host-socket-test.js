const WebSocket = require('ws');
const Socket = require('./node/sockets/socket')
//const ws = new WebSocket('ws://161.35.201.51:3000');
const ws = new WebSocket('ws://localhost:3000');
const socket = new Socket(ws)

var playerData = {
    "playerID"          : "1",
    "nickname"          : "HosterMan",
    "characterClass"    : "mage",
    "currentHealth"     : 8,
    "maxHealth"         : 8,
    "currentEnergy"     : 5,
    "maxEnergy"         : 5,
    "attackValue"       : 2,
    "ready"             : false
}

socket.on('connect', () => {
    console.log('Connected!')
    socket.emit('host-game', playerData)

    socket.on('game-joined', (game) => {
        console.log('Game Hosted:')
        console.log(game)
        socket.emit('set-player-ready', true)
    })

    socket.on('player-left', (playerID, game) => {
        console.log('Player left: ' + playerID)
        console.log(game)
    })
    
    socket.on('player-joined', (playerID, game) => {
        console.log('Player joined: ' + playerID)
        console.log(game)
    })
    
    socket.on('game-error', (err) => {
        console.log("Error: " + err)
    })

    socket.on('player-update', (playerData) => {
        socket.emit('start-game', true)
    })

    socket.on('game-started', (adventure, options) => {
        console.log('Game started')
        console.log(adventure)
        console.log(options)
    })
})